var rand = require('csprng')

var accounts = []

/* A */
function authorisationSent(auth) {
  console.log('CHECK AUTH PRESENT')
  return new Promise(function(resolve, reject) {
    console.log('  a')
    if (auth.scheme !== 'Basic') {
      console.log('  b')
      console.log('basic auth not supplied')
      return reject({code: 401, response:{ status:'error', message:'Basic access authentication required'} })
    }
    if (auth.basic.username == undefined || auth.basic.password == undefined) {
      console.log('  c')
      console.log('missing username or password')
      return reject({code: 401, response:{ status:'error', message:'missing username or password'} })
    }
    console.log('  d')
    return resolve({code: 200, response:{ status:'success', message:'authorization header has been supplied'} })
  })
}

/* B */
function checkUniqueUsername(auth) {
  console.log('CHECK UNIQUE USERNAME')
  return new Promise(function(resolve, reject) {
    console.log(typeof auth)
    const username = auth.basic.username
    for (var i=0; i<accounts.length; i++) {
      if (accounts[i].username === auth.basic.username) {
        return reject({code: 409, response:{ status:'error', message:'username already exists' }})
      }
    }
    return resolve({code: 200, response:{ status:'success', message:'username is not already in use' }})
  })
}

/* C */
function checkBody(body) {
  console.log('CHECK BODY')
  return new Promise(function(resolve, reject) {
    console.log('  a')
    console.log(JSON.stringify(body, null, 2))
    if (typeof body.name !== 'string' || typeof body.email !== 'string') {
      console.log('  b')
      return reject({code: 400, response:{status:'error', message:'name and email body fields required'}})
    }
    return resolve({code: 200, response:{status:'success', message:'name and email body fields present'}})
  })
}

/* D */
function buildDocument(body) {
  console.log('BUILD ACCOUNT DOCUMENT')
  return new Promise(function(resolve, reject) {
    const user = {name: body.name, email:body.email}
    console.log(user)
    return resolve({code: 200, response:{status:'success', message:'document created', data:user}})
  })
}

/* E */
function addAccount(user) {
  console.log('ADD ACCOUNT')
  return new Promise(function(resolve, reject) {
    const newId = rand(160, 36)
    const doc = {id:newId, user:user}
    accounts.push(doc)
    return resolve({code: 200, response:{status:'success', message:'document created', data:doc}})
  })
}

/* This function handles adding a new user account. This is a complex process that requires a lot of different steps. Many steps have the chance of failure. If a step fails the entire process should fail. If we didn't use promises the level of nesting would be unmanageable. */
exports.add = function(host, auth, body, files, callback) {
  var doc, id  // these variables will be used to store data returned by step in the process.
  console.log('A')
  authorisationSent(auth).then(function() {
    console.log('B')
    return checkUniqueUsername(auth)
  }).then(function() {
    console.log('C')
    return checkBody(body)
  }).then(function() {
    console.log('D')
    return buildDocument(body)
  }).then(function(data) {
    console.log('E')
    callback(data)
  })
}
