var request = require('request')
// This is the varaible where the books will be stored
var books = 'bookshelf';
var ID = 'ID';
var result = 'result';


/* this function validates the JSON data in the API */
function validateJson(json) {
  if (typeof json.name !== 'string') {
    console.log('name not a string')
    return false
  }
  /* if the list key is not an aray it will fail and log that it is not an array */
  if (!Array.isArray(json.list)) {
    console.log('json.list is not an array')
    return false
  }
  for(var i=0; i<json.list.length; i++) {
    if (typeof json.list[i] !== 'string') {
      console.log('not a string')
      return false
    }
  }
  return true

/*
exports.search = function(query, callback) {
  console.log('search')
  if (typeof query !== 'string' || query.length === 0) {
    callback({code:400, response:{status:'error', message:'missing query (q parameter)'}})
  }
  // The url and key. n:50 is for testing
  request.get({url: 'https://www.googleapis.com/books/v1/volumes/?akey=AIzaSyDf1YqqxMuVqyH5mbGfNcDwh3Q1_Fy9DZ8', qs: {t: books, n:50 }}, function (error, response, body) { 
    if (!error && response.statusCode == 200) { // success code
      console.log(body); 
      console.log(typeof body)
      const json = JSON.parse(body)
      const data = books.getByID(ID, function(result){
      console.log(result)})
      const books = json.books.map(function(element) {
          // This is the information that will be sent to the client when get function runs.
        //return {status:'success', contentType:'application/json', message:'ID of books found', data: result.items[0]}
      })
      console.log(books)
      // this is the call back function to log information for the devloper.
      callback({code:200, response:{status:'success', message:books.length+' books found', data: result.items[0]}})
    }
  })
}
/* 

}
/* This witll get data input by the user on the client and arrange the URL based on what they put, first withgetting what they input, the adding the API key and giving a 200 reponse if sucsessful */
exports.getByID = function(id, callback) {
  console.log('getById: '+id)
  var qs = {q: "inid:"+id, key: "AIzaSyDf1YqqxMuVqyH5mbGfNcDwh3Q1_Fy9DZ8"};
  request({uri: 'https://www.googleapis.com/books/v1/volumes', qs: qs}, function (error, response, body) {
    console.log("error", error);
    console.log("body", body);
  if (!error && response.statusCode == 200) {
    var result = JSON.parse(body)
    // if it is sucsessfull it will retun a console log of the following details. The Data: "result.items[0]}}" is what the user input.
    callback({code:200, response:{status:'success', contentType:'application/json', message:'ID of books found', data: result.items[0]}})
  }
})
  /* 406 error returned if the thing is incorrect */
  return {code:406, response:{status:'error', contentType:'application/json', message:'ID of book not found', data: id}}
}
